package com.tuuzed.android.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rengwuxian.materialedittext.MaterialEditText;

public class InputDialog extends BaseDialog<InputDialog.Builder> {

    private MaterialEditText mEtInput;

    @Nullable
    private Callback callback = null;

    public static InputDialog newInstance(@NonNull Builder params) {
        InputDialog fragment = new InputDialog();
        fragment.setParams(params);
        fragment.callback = params.callback;
        return fragment;
    }

    @Nullable
    @Override
    public View onContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                              @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.input_layout, container, false);
        initView(view);
        return view;
    }

    @Override
    protected View.OnClickListener getPositiveClickListener() {
        return v -> {
            if (callback != null) {
                callback.call(mEtInput.getText().toString());
            }
            if (mParams.positiveClickListener != null) {
                mParams.positiveClickListener.onClick(getDialog(), DialogInterface.BUTTON_POSITIVE);
            }
        };
    }

    private void initView(View view) {
        mEtInput = view.findViewById(R.id.et_input);
        // initEditText
        mEtInput.setText(mParams.preInput);
        mEtInput.setInputType(mParams.inputType);
        mEtInput.setSelection(mParams.preInput.length());
        if (mParams.maxLength > 0) mEtInput.setMaxCharacters(mParams.maxLength);
        if (mParams.minLength > 0) mEtInput.setMinCharacters(mParams.minLength);
        if (mParams.helperText != null) mEtInput.setHelperText(mParams.helperText);
    }

    public static class Builder extends BaseDialog.Builder<Builder, InputDialog> {
        private String preInput;
        private int inputType = InputType.TYPE_CLASS_TEXT;
        private int maxLength = -1;
        private int minLength = -1;
        private String helperText;
        private Callback callback = null;

        Builder(@NonNull Context context) {
            super(context);
        }

        @NonNull
        @Override
        public InputDialog create() {
            if (preInput == null) preInput = "";
            return InputDialog.newInstance(this);
        }

        public Builder preInput(@StringRes int resId) {
            return preInput(context.getString(resId));
        }

        public Builder preInput(String preInput) {
            this.preInput = preInput;
            return this;
        }

        public Builder inputType(int inputType) {
            this.inputType = inputType;
            return this;
        }

        public Builder maxLength(int maxLength) {
            this.maxLength = maxLength;
            return this;
        }

        public Builder minLength(int minLength) {
            this.minLength = minLength;
            return this;
        }

        public Builder helperText(@StringRes int resId) {
            return helperText(context.getString(resId));
        }

        public Builder helperText(String helperText) {
            this.helperText = helperText;
            return this;
        }

        public Builder callback(Callback callback) {
            this.callback = callback;
            return this;
        }
    }

    public interface Callback {
        void call(String text);
    }
}
