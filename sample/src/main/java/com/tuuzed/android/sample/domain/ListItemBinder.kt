package com.tuuzed.android.sample.domain

import android.view.View
import com.tuuzed.android.adapter.BaseItemViewBinder
import com.tuuzed.android.adapter.ItemViewHolder
import com.tuuzed.android.sample.R

class ListItemBinder : BaseItemViewBinder<ListItem>() {
    override fun getLayoutId(): Int = R.layout.list_item_layout

    override fun onBindViewHolder(holder: ItemViewHolder, item: ListItem, position: Int) {
        if (item.listener == null) {
            holder.find<View>(R.id.iv_right_icon).visibility = View.GONE
            holder.itemView.setOnClickListener(null)
        } else {
            holder.find<View>(R.id.iv_right_icon).visibility = View.VISIBLE
            holder.itemView.setOnClickListener { item.listener?.onListItemClick(holder, item, position) }
        }
        holder.text(R.id.tv_title, item.title)
    }
}
