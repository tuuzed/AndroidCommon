package com.tuuzed.android.ui;

import android.support.annotation.NonNull;
import android.widget.ImageView;

public interface ImageLoader {
    void displayImage(@NonNull String url, @NonNull ImageView imageView);
}