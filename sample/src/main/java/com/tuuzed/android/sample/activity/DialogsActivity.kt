package com.tuuzed.android.sample.activity

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.tuuzed.android.adapter.RecyclerViewAdapter
import com.tuuzed.android.adapter.RecyclerViewDivider
import com.tuuzed.android.base.BaseActivity
import com.tuuzed.android.dialog.DialogBuilderProxy
import com.tuuzed.android.sample.R
import com.tuuzed.android.sample.domain.ListItem
import com.tuuzed.android.sample.domain.ListItemBinder
import com.tuuzed.android.sample.domain.OnListItemClickListener
import com.tuuzed.android.utilkt.toastShort
import com.tuuzed.android.viewholder.ViewHolder
import com.tuuzed.android.viewholder.ViewHolderFactory
import java.text.SimpleDateFormat
import java.util.*

class DialogsActivity : BaseActivity() {

    private lateinit var mViewHolder: ViewHolder
    private lateinit var mDialogsAdapter: RecyclerViewAdapter
    private lateinit var mContext: Context

    override fun isHomeAsUpEnabled(): Boolean = true
    override fun isSwipeBackEnable(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialogs_act)
        mContext = this
        initViews()
        initListItem()
    }

    private fun initViews() {
        mViewHolder = ViewHolderFactory.of(this)
        setSupportActionBar(mViewHolder.find(R.id.toolbar))
        mDialogsAdapter = RecyclerViewAdapter().register(ListItem::class.java, ListItemBinder())
                .attach(mViewHolder.find(R.id.rv_dialogs))
        mViewHolder.find(R.id.rv_dialogs, RecyclerView::class.java)
                .addItemDecoration(RecyclerViewDivider.linearLayout(this, LinearLayoutManager.HORIZONTAL))
    }

    private fun initListItem() {
        mDialogsAdapter
                .addItem(ListItem("InputDialog", OnListItemClickListener { _, item, _ ->
                    DialogBuilderProxy.newProxy(mContext)
                            .title(item.title)
                            .positive(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
                            .negative(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
                            .cancelable(false)
                            .input()
                            .preInput(item.title)
                            .minLength(10).maxLength(50)
                            .helperText(item.title)
                            .callback { toastShort("Input: $it") }
                            .show(supportFragmentManager, item.title)
                }))
                .addItem(ListItem("DatePickerDialog", OnListItemClickListener { _, item, _ ->
                    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA)
                    DialogBuilderProxy.newProxy(mContext)
                            .icon(R.drawable.ic_warning)
                            .title(item.title)
                            .positive(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
                            .negative(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
                            .cancelable(false)
                            .datePicker()
                            .rangeYear(2000, 2050)
                            .listener { toastShort(dateFormat.format(it)) }
                            .callback { toastShort("Selected: ${dateFormat.format(it)}") }
                            .show(supportFragmentManager, item.title)
                }))
                .addItem(ListItem("DoubleDatePickerDialog", OnListItemClickListener { _, item, _ ->
                    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA)
                    DialogBuilderProxy.newProxy(mContext)
                            .title(item.title, resources.getColor(android.R.color.white))
                            .titleBackgroundColor(resources.getColor(R.color.colorPrimary))
                            .positive(android.R.string.ok, resources.getColor(R.color.colorAccent)) { dialog, _ -> dialog.dismiss() }
                            .negative(android.R.string.cancel, resources.getColor(R.color.thirdText)) { dialog, _ -> dialog.dismiss() }
                            .cancelable(false)
                            .doubleDatePicker()
                            .rangeYear(2000, 2050)
                            .callback { beginDate, endDate ->
                                toastShort("Selected: ${dateFormat.format(beginDate)} ~ ${dateFormat.format(endDate)}")
                            }
                            .show(supportFragmentManager, item.title)
                }))
                .addItem(ListItem("MessageDialog", OnListItemClickListener { _, item, _ ->
                    DialogBuilderProxy.newProxy(mContext)
                            .title(item.title)
                            .positive(android.R.string.ok, resources.getColor(R.color.dialog_thirdText)) { dialog, _ -> dialog.dismiss() }
                            .cancelable(false)
                            .message("This Message .")
                            .show(supportFragmentManager, item.title)
                }))
                .addItem(ListItem("ProgressBarDialog", OnListItemClickListener { _, item, _ ->
                    DialogBuilderProxy.newProxy(mContext)
                            .title(item.title)
                            .negative(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
                            .cancelable(false)
                            .progressBar("Please wait .")
                            .show(supportFragmentManager, item.title)
                }))
                .addItem(ListItem("CancelableProgressBarDialog", OnListItemClickListener { _, item, _ ->
                    DialogBuilderProxy.newProxy(mContext)
                            .title(item.title)
                            .progressBar("Please wait .")
                            .show(supportFragmentManager, item.title)
                }))
                .addItem(ListItem("ListDialog", OnListItemClickListener { _, item, _ ->
                    DialogBuilderProxy.newProxy(mContext)
                            .items(R.array.items)
                            .singleChoose("") { toastShort("Selected: $it") }
                            .hideButton()
                            .show(supportFragmentManager, item.title)
                }))
                .addItem(ListItem("SingleChooseListDialog", OnListItemClickListener { _, item, _ ->
                    DialogBuilderProxy.newProxy(mContext)
                            .title(item.title)
                            .negative(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
                            .items(R.array.items)
                            .singleChoose("Item1") { toastShort("Selected: $it") }
                            .show(supportFragmentManager, item.title)
                }))
                .addItem(ListItem("MultiChooseListDialog", OnListItemClickListener { _, item, _ ->
                    DialogBuilderProxy.newProxy(mContext)
                            .title(item.title)
                            .positive(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
                            .negative(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
                            .items(R.array.items)
                            .multiChoose(arrayOf("Item1", "Item3")) { toastShort("Selected: $it") }
                            .show(supportFragmentManager, item.title)
                }))
                .addItem(ListItem("CustomViewDialog", OnListItemClickListener { _, item, _ ->
                    DialogBuilderProxy.newProxy(mContext)
                            .title(item.title)
                            .positive(android.R.string.ok) { dialog, _ -> dialog.dismiss() }
                            .negative(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
                            .contentView(R.layout.custom_view_dialog)
                            .show(supportFragmentManager, item.title)
                })).notifyDataSetChanged()
    }
}