package com.tuuzed.android.sample.activity

import android.os.Bundle
import android.widget.TextView
import com.tuuzed.android.base.BaseActivity
import com.tuuzed.android.sample.R
import com.tuuzed.android.ui.BottomTabGroup

class BottomTabActivity : BaseActivity() {

    private lateinit var mTvMsg: TextView
    private lateinit var mBottomTabGroup: BottomTabGroup

    override fun isHomeAsUpEnabled(): Boolean = true
    override fun isSwipeBackEnable(): Boolean = true


    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        setContentView(R.layout.bottom_tab_act)
        setSupportActionBar(findViewById(R.id.toolbar))
        mTvMsg = findViewById(R.id.tv_msg)
        mBottomTabGroup = findViewById(R.id.bottom_tag_group)

        mBottomTabGroup.setOnTabCheckedListener { isClick, checkId ->
            when (checkId) {
                R.id.tab1 -> mTvMsg.text = "tab1"
                R.id.tab2 -> mTvMsg.text = "tab2"
                R.id.tab3 -> mTvMsg.text = "tab3"
                R.id.tab4 -> mTvMsg.text = "tab4"
            }
            if (isClick) {
                mBottomTabGroup.enableBadge(checkId, false)
            }
        }
        mBottomTabGroup.setCheckedId(R.id.tab3)
        mBottomTabGroup.enableBadge(R.id.tab3, true)
    }
}
