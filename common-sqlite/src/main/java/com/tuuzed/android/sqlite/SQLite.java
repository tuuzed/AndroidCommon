package com.tuuzed.android.sqlite;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.tuuzed.android.sqlite.args.CountArgs;
import com.tuuzed.android.sqlite.args.FindArgs;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public interface SQLite {

    <T> boolean exists(@NonNull T t);

    <T> boolean save(@NonNull T t);

    <T> boolean insert(@NonNull T t);

    long insertInTx(@NonNull Collection<?> objects);

    long insertInTx(@NonNull Collection<?> objects, boolean errorContinue);

    <T> boolean insertOrThrow(@NonNull T t) throws SQLException;

    <T> boolean update(@NonNull T t);

    long updateInTx(@NonNull Collection<?> objects);

    long updateInTx(@NonNull Collection<?> objects, boolean errorContinue);

    <T> long updateByCondition(@NonNull T t, @Nullable String whereClause, @Nullable String[] whereArgs);

    long updateByCondition(@NonNull Class<?> tableClass, @NonNull String[] columns, @NonNull Object[] values, @Nullable String whereClause, @Nullable String[] whereArgs);

    <T> boolean delete(@NonNull T t);

    int deleteByCondition(@NonNull Class<?> tableClass, @Nullable String whereClause, @Nullable String[] whereArgs);

    int deleteAll(@NonNull Class<?> tableClass);

    @Nullable
    <T> T findOne(@NonNull Class<T> tableClass);

    @Nullable
    <T> T findOne(@NonNull Class<T> tableClass, @Nullable String whereClause, @Nullable String[] whereArgs);

    @Nullable
    <T> T findOne(@NonNull FindArgs<T> findArgs);

    @NonNull
    <T> List<T> findAsList(@NonNull Class<T> tableClass);

    @NonNull
    <T> List<T> findAsList(@NonNull Class<T> tableClass, @Nullable String whereClause, @Nullable String[] whereArgs);

    @NonNull
    <T> List<T> findAsList(@NonNull FindArgs<T> findArgs);

    @NonNull
    <T> Iterator<T> findAsIterator(@NonNull Class<T> tableClass);

    @NonNull
    <T> Iterator<T> findAsIterator(@NonNull Class<T> tableClass, @Nullable String whereClause, @Nullable String[] whereArgs);

    @NonNull
    <T> Iterator<T> findAsIterator(@NonNull FindArgs<T> findArgs);

    long count(@NonNull Class<?> tableClass);

    long count(@NonNull Class<?> tableClass, @Nullable String whereClause, @Nullable String[] whereArgs);

    long count(@NonNull CountArgs<?> countArgs);

    @NonNull
    SQLiteDatabase getWritableDatabase();

    @NonNull
    SQLiteDatabase getReadableDatabase();

    void destroy();
}
