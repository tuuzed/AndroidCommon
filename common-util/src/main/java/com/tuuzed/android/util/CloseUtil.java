package com.tuuzed.android.util;

import android.support.annotation.Nullable;

import java.io.Closeable;
import java.io.IOException;

public final class CloseUtil {

    private static final String TAG = "CloseUtil";

    public static void safeClose(@Nullable Closeable closeable) {
        if (closeable == null) return;
        try {
            closeable.close();
        } catch (IOException e) {
            LogUtil.e(TAG, "safeClose IOException", e);
        }
    }

    public static void safeClose(@Nullable Closeable... closeables) {
        if (closeables == null) return;
        for (Closeable c : closeables) safeClose(c);
    }
}
