package com.tuuzed.android.viewholder;

import android.app.Dialog;
import android.view.View;


class DialogViewHolder extends AbstractViewHolder implements ViewHolder {
    private Dialog mDialog;

    DialogViewHolder(Dialog target) {
        super(16);
        this.mDialog = target;
    }

    @Override
    <T extends View> T findViewById(int id) {
        return mDialog.findViewById(id);
    }

    @Override
    public void clear() {
        super.clear();
        mDialog = null;
    }
}