package com.tuuzed.android.viewholder;

import android.app.Activity;
import android.view.View;


class ActivityViewHolder extends AbstractViewHolder {
    private Activity mActivity;

    ActivityViewHolder(Activity target) {
        super(16);
        this.mActivity = target;
    }

    @Override
    <T extends View> T findViewById(int id) {
        return mActivity.findViewById(id);
    }

    @Override
    public void clear() {
        super.clear();
        mActivity = null;
    }
}