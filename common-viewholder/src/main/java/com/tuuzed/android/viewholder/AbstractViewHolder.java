package com.tuuzed.android.viewholder;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


abstract class AbstractViewHolder implements ViewHolder {
    private SparseArray<View> mViews;

    AbstractViewHolder(int initialCapacity) {
        this.mViews = new SparseArray<>(initialCapacity);
    }

    abstract <T extends View> T findViewById(@IdRes int id);

    @Override
    public <T extends View> T find(@IdRes int id) {
        View view = mViews.get(id);
        if (view == null) {
            view = findViewById(id);
            if (view == null) throw new IllegalArgumentException("find# id not find");
            mViews.put(id, view);
        }
        //noinspection unchecked
        return (T) view;
    }

    @Override
    public <T extends View> T find(@IdRes int id, @NonNull Class<T> type) {
        return find(id);
    }

    @Override
    public CharSequence text(@IdRes int id) {
        View view = find(id);
        if (view instanceof TextView) {
            return ((TextView) view).getText();
        }
        return null;
    }

    @Override
    public void text(@IdRes int id, @NonNull CharSequence text) {
        View view = find(id);
        if (view instanceof TextView) {
            ((TextView) view).setText(text);
        }
    }

    @Override
    public void text(@IdRes int id, int resId) {
        View view = find(id);
        if (view instanceof TextView) {
            ((TextView) view).setText(resId);
        }
    }

    @Override
    public void image(@IdRes int id, @DrawableRes int resId) {
        View view = find(id);
        if (view instanceof ImageView) {
            ((ImageView) view).setImageResource(resId);
        }
    }

    @Override
    public void image(@IdRes int id, @NonNull Bitmap bitmap) {
        View view = find(id);
        if (view instanceof ImageView) {
            ((ImageView) view).setImageBitmap(bitmap);
        }
    }

    @Override
    public void image(@IdRes int id, @NonNull Drawable drawable) {
        View view = find(id);
        if (view instanceof ImageView) {
            ((ImageView) view).setImageDrawable(drawable);
        }
    }

    @Override
    public void apply(@IdRes int id, @NonNull Action action) {
        action.apply(find(id), 0);
    }

    @Override
    public void apply(int[] ids, @NonNull Action action) {
        for (int i = 0; i < ids.length; i++) {
            action.apply(find(ids[i]), i);
        }
    }

    @Override
    public void click(@IdRes int id, @NonNull View.OnClickListener listener) {
        find(id).setOnClickListener(listener);
    }

    @Override
    public void click(int[] ids, @NonNull View.OnClickListener listener) {
        for (int id : ids) {
            find(id).setOnClickListener(listener);
        }
    }

    @Override
    public void longClick(@IdRes int id, @NonNull View.OnLongClickListener listener) {
        find(id).setOnLongClickListener(listener);
    }

    @Override
    public void longClick(@IdRes int[] ids, @NonNull View.OnLongClickListener listener) {
        for (int id : ids) {
            find(id).setOnLongClickListener(listener);
        }
    }

    @Override
    public void clear() {
        mViews.clear();
    }
}
