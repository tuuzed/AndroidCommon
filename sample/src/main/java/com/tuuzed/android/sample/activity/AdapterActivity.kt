package com.tuuzed.android.sample.activity

import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import com.tuuzed.android.adapter.BaseItemViewBinder
import com.tuuzed.android.adapter.ItemViewHolder
import com.tuuzed.android.adapter.RecyclerViewAdapter
import com.tuuzed.android.adapter.RecyclerViewDivider
import com.tuuzed.android.base.BaseActivity
import com.tuuzed.android.sample.R
import com.tuuzed.android.utilkt.toastShort
import com.tuuzed.android.viewholder.ViewHolder
import com.tuuzed.android.viewholder.ViewHolderFactory
import kotlinx.android.synthetic.main.adapter_act.*

class AdapterActivity : BaseActivity() {

    private lateinit var mViewHolder: ViewHolder
    private lateinit var mLinearAdapter: RecyclerViewAdapter
    private lateinit var mGridAdapter: RecyclerViewAdapter
    private lateinit var mStaggeredGridAdapter: RecyclerViewAdapter

    override fun isHomeAsUpEnabled(): Boolean = true
    override fun isSwipeBackEnable(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.adapter_act)
        initViews()
        initData()
    }

    private fun initData() {
        var staggeredTitle = ""
        for (i in 0..10) {
            mLinearAdapter.addItem(ModuleItem("Item$i", View.OnClickListener {
                toastShort("Item$i")
            }))
            mGridAdapter.addItem(ModuleItem("Item$i", View.OnClickListener {
                toastShort("Item$i")
            }))
            staggeredTitle += "Item\t"
            mStaggeredGridAdapter.addItem(ModuleItem("$staggeredTitle$i", View.OnClickListener {
                toastShort("$staggeredTitle$i")
            }))
        }
    }


    private fun initViews() {
        mViewHolder = ViewHolderFactory.of(this)
        setSupportActionBar(mViewHolder.find(R.id.toolbar))
        mLinearAdapter = RecyclerViewAdapter()
                .register(ModuleItem::class.java, object : BaseItemViewBinder<ModuleItem>() {
                    override fun getLayoutId(): Int = R.layout.adapter_item

                    override fun onBindViewHolder(holder: ItemViewHolder, item: ModuleItem, position: Int) {
                        holder.text(R.id.tv_module_name, item.title)
                        holder.itemView.setOnClickListener(item.onClick)
                    }
                })
                .attach(mViewHolder.find(R.id.rv_linear))
        rv_linear.addItemDecoration(RecyclerViewDivider.linearLayout(RecyclerView.HORIZONTAL, ContextCompat.getDrawable(this, R.drawable.divider)!!))

        mGridAdapter = RecyclerViewAdapter()
                .register(ModuleItem::class.java, object : BaseItemViewBinder<ModuleItem>() {
                    override fun getLayoutId(): Int = R.layout.adapter_item
                    override fun onBindViewHolder(holder: ItemViewHolder, item: ModuleItem, position: Int) {
                        holder.text(R.id.tv_module_name, item.title)
                        holder.itemView.setOnClickListener(item.onClick)
                    }
                })
                .attach(mViewHolder.find(R.id.rv_grid))
        rv_grid.addItemDecoration(RecyclerViewDivider.gridLayout(this))

        mStaggeredGridAdapter = RecyclerViewAdapter()
                .register(ModuleItem::class.java, object : BaseItemViewBinder<ModuleItem>() {
                    override fun getLayoutId(): Int = R.layout.adapter_staggered_item
                    override fun onBindViewHolder(holder: ItemViewHolder, item: ModuleItem, position: Int) {
                        holder.text(R.id.tv_module_name, item.title)
                        holder.itemView.setOnClickListener(item.onClick)
                    }
                })
                .attach(mViewHolder.find(R.id.rv_staggered_grid))
        rv_staggered_grid.addItemDecoration(RecyclerViewDivider.gridLayout(5, Color.BLUE))

    }

    data class ModuleItem(var title: String, var onClick: View.OnClickListener)
}
