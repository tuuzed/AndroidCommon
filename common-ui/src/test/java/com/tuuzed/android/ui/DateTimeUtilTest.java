package com.tuuzed.android.ui;

import com.tuuzed.android.ui.internal.DateTimeUtil;

import org.junit.Test;

public class DateTimeUtilTest {

    @Test
    public void getLastDayByYearMonth() {
        int lastDayByYearMonth = DateTimeUtil.getLastDayByYearMonth(1970, 2);
        System.out.println(lastDayByYearMonth);
    }

    @Test
    public void currentYear() {
        int currentYear = DateTimeUtil.currentYear();
        System.out.println(currentYear);
    }

    @Test
    public void currentMonth() {
        int currentMonth = DateTimeUtil.currentMonth();
        System.out.println(currentMonth);
    }

    @Test
    public void currentDay() {
        int currentDay = DateTimeUtil.currentDay();
        System.out.println(currentDay);
    }
}