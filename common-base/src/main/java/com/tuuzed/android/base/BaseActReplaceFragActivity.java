package com.tuuzed.android.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

public abstract class BaseActReplaceFragActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_act_replace_frag);
        setSupportActionBar(findViewById(R.id.toolbar));
    }
}
