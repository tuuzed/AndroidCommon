package com.tuuzed.android.utilkt

import android.app.Activity
import android.content.Context
import android.preference.Preference
import android.support.annotation.IdRes
import android.support.annotation.NonNull
import android.support.v7.app.AppCompatActivity
import com.tuuzed.android.util.*
import java.io.Closeable

// ActivityUtil

fun Activity.replaceFragmentTo(@IdRes containerViewId: Int,
                               @NonNull fragment: android.app.Fragment,
                               @NonNull tag: String = fragment::class.java.simpleName) =
        ActivityUtil.replaceFragmentTo(fragmentManager, containerViewId, fragment, tag)

fun AppCompatActivity.replaceSupportFragmentTo(@IdRes containerViewId: Int,
                                               @NonNull fragment: android.support.v4.app.Fragment,
                                               @NonNull tag: String = fragment::class.java.simpleName) =
        ActivityUtil.replaceSupportFragmentTo(supportFragmentManager, containerViewId, fragment, tag)

// Preference

fun Preference.getBoolean(defValue: Boolean) = sharedPreferences.getBoolean(key, defValue)
fun Preference.getFloat(defValue: Float) = sharedPreferences.getFloat(key, defValue)
fun Preference.getInt(defValue: Int) = sharedPreferences.getInt(key, defValue)
fun Preference.getLong(defValue: Long) = sharedPreferences.getLong(key, defValue)
fun Preference.getString(defValue: String) = sharedPreferences.getString(key, defValue)
fun Preference.getStringSet(defValue: Set<String>) = sharedPreferences.getStringSet(key, defValue)

// AppSharedPreferenceUtil

fun Context.getAppSpItem(key: String, defValue: Boolean) = AppSharedPreferenceUtil.getAppSpItem(this, key, defValue)
fun Context.getAppSpItem(key: String, defValue: Float) = AppSharedPreferenceUtil.getAppSpItem(this, key, defValue)
fun Context.getAppSpItem(key: String, defValue: Int) = AppSharedPreferenceUtil.getAppSpItem(this, key, defValue)
fun Context.getAppSpItem(key: String, defValue: Long) = AppSharedPreferenceUtil.getAppSpItem(this, key, defValue)
fun Context.getAppSpItem(key: String, defValue: String) = AppSharedPreferenceUtil.getAppSpItem(this, key, defValue)
fun Context.getAppSpItem(key: String, defValue: Set<String>) = AppSharedPreferenceUtil.getAppSpItem(this, key, defValue)
fun Context.setAppSpItem(key: String, value: Boolean) = AppSharedPreferenceUtil.setAppSpItem(this, key, value)
fun Context.setAppSpItem(key: String, value: Float) = AppSharedPreferenceUtil.setAppSpItem(this, key, value)
fun Context.setAppSpItem(key: String, value: Int) = AppSharedPreferenceUtil.setAppSpItem(this, key, value)
fun Context.setAppSpItem(key: String, value: Long) = AppSharedPreferenceUtil.setAppSpItem(this, key, value)
fun Context.setAppSpItem(key: String, value: String) = AppSharedPreferenceUtil.setAppSpItem(this, key, value)
fun Context.setAppSpItem(key: String, value: Set<String>) = AppSharedPreferenceUtil.setAppSpItem(this, key, value)

// CloseUtil

fun safeClose(closeable: Closeable?) = CloseUtil.safeClose(closeable)
fun safeClose(vararg closeables: Closeable?) = closeables.forEach { CloseUtil.safeClose(it) }

// LogUtil

fun LOGV(tag: String, msg: String) = LogUtil.v(tag, msg)
fun LOGD(tag: String, msg: String) = LogUtil.d(tag, msg)
fun LOGI(tag: String, msg: String) = LogUtil.i(tag, msg)
fun LOGW(tag: String, msg: String) = LogUtil.w(tag, msg)
fun LOGE(tag: String, msg: String) = LogUtil.e(tag, msg)
fun LOGV(tag: String, msg: String, tr: Throwable?) = LogUtil.v(tag, msg, tr)
fun LOGD(tag: String, msg: String, tr: Throwable?) = LogUtil.d(tag, msg, tr)
fun LOGI(tag: String, msg: String, tr: Throwable?) = LogUtil.i(tag, msg, tr)
fun LOGW(tag: String, msg: String, tr: Throwable?) = LogUtil.w(tag, msg, tr)
fun LOGE(tag: String, msg: String, tr: Throwable?) = LogUtil.e(tag, msg, tr)
fun Any.LOGV(msg: String) = LogUtil.v(this::class.java.simpleName, msg)
fun Any.LOGD(msg: String) = LogUtil.d(this::class.java.simpleName, msg)
fun Any.LOGI(msg: String) = LogUtil.i(this::class.java.simpleName, msg)
fun Any.LOGW(msg: String) = LogUtil.w(this::class.java.simpleName, msg)
fun Any.LOGE(msg: String) = LogUtil.e(this::class.java.simpleName, msg)
fun Any.LOGV(msg: String, tr: Throwable?) = LogUtil.v(this::class.java.simpleName, msg, tr)
fun Any.LOGD(msg: String, tr: Throwable?) = LogUtil.d(this::class.java.simpleName, msg, tr)
fun Any.LOGI(msg: String, tr: Throwable?) = LogUtil.i(this::class.java.simpleName, msg, tr)
fun Any.LOGW(msg: String, tr: Throwable?) = LogUtil.w(this::class.java.simpleName, msg, tr)
fun Any.LOGE(msg: String, tr: Throwable?) = LogUtil.e(this::class.java.simpleName, msg, tr)
fun LOGW(tag: String, tr: Throwable?) = LogUtil.w(tag, tr)
fun Any.LOGW(tr: Throwable?) = LogUtil.w(this::class.java.simpleName, tr)

// MainHandlerUtil

fun runOnUiThread(r: Runnable) = MainHandlerUtil.runOnUiThread(r)
fun runOnMainThread(r: Runnable) = MainHandlerUtil.runOnMainThread(r)

// ToastUtil

fun Context.toastShort(text: CharSequence) = ToastUtil.showShort(this, text)
fun Context.toastLong(text: CharSequence) = ToastUtil.showLong(this, text)
fun Context.showShort(text: CharSequence) = ToastUtil.showShort(this, text)
fun Context.showLong(text: CharSequence) = ToastUtil.showLong(this, text)
