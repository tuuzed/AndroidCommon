package com.tuuzed.android.sqlite.args;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.tuuzed.android.sqlite.UpgradeCallback;
import com.tuuzed.android.sqlite.annotation.Database;
import com.tuuzed.android.sqlite.internal.StringUtil;

/**
 * 数据库参数
 */
public class DatabaseArgs {
    public final Class<?> databaseClass;
    public final int databaseVersion;
    public final String databaseName;
    public final boolean defaultDatabase;
    public final boolean echoSQL;
    public final Class<?>[] tableClasses;
    private UpgradeCallback upgradeCallback;
    private SQLiteDatabase.CursorFactory cursorFactory;


    @NonNull
    public static DatabaseArgs of(@NonNull Class<?> databaseClass) {
        return new DatabaseArgs(databaseClass);
    }

    private DatabaseArgs(@NonNull Class<?> databaseClass) {
        this.databaseClass = databaseClass;
        Database database = databaseClass.getAnnotation(Database.class);
        if (database == null) {
            throw new IllegalArgumentException("arg databaseClass not Database");
        }
        this.databaseVersion = database.version();
        this.databaseName = TextUtils.isEmpty(database.name()) ?
                StringUtil.camelToUnderline(databaseClass.getSimpleName()) + ".db" : database.name();
        this.defaultDatabase = database.isDefault();
        this.tableClasses = database.tables();
        this.echoSQL = database.echoSQL();
    }

    @NonNull
    public DatabaseArgs cursorFactory(@NonNull SQLiteDatabase.CursorFactory cursorFactory) {
        this.cursorFactory = cursorFactory;
        return this;
    }

    @NonNull
    public DatabaseArgs upgradeCallback(@NonNull UpgradeCallback upgradeCallback) {
        this.upgradeCallback = upgradeCallback;
        return this;
    }

    @Nullable
    public UpgradeCallback upgradeCallback() {
        return upgradeCallback;
    }

    @Nullable
    public SQLiteDatabase.CursorFactory cursorFactory() {
        return cursorFactory;
    }


}
