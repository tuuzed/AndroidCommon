package com.tuuzed.android.sample.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.tuuzed.android.adapter.RecyclerViewAdapter
import com.tuuzed.android.adapter.RecyclerViewDivider
import com.tuuzed.android.base.BaseActivity
import com.tuuzed.android.sample.R
import com.tuuzed.android.sample.domain.ListItem
import com.tuuzed.android.sample.domain.ListItemBinder
import com.tuuzed.android.sample.domain.OnListItemClickListener
import com.tuuzed.android.utilkt.toastShort
import com.tuuzed.android.viewholder.ViewHolder
import com.tuuzed.android.viewholder.ViewHolderFactory

class MainActivity : BaseActivity() {

    private lateinit var mViewHolder: ViewHolder
    private lateinit var mModuleAdapter: RecyclerViewAdapter
    private lateinit var mContext: Context


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_act)
        mContext = this
        initViews()
        initListItem()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SCAN_QRCODE_REQUEST_CODE
                && resultCode == Activity.RESULT_OK
                && data != null) {
            val result = data.getStringExtra(CaptureActivity.EXTRA_KEY_SCAN_RESULT)
            toastShort(result)
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun initViews() {
        mViewHolder = ViewHolderFactory.of(this)
        setSupportActionBar(mViewHolder.find(R.id.toolbar))
        mModuleAdapter = RecyclerViewAdapter()
                .register(ListItem::class.java, ListItemBinder())
                .attach(mViewHolder.find(R.id.rv_module))
        mViewHolder.find(R.id.rv_module, RecyclerView::class.java)
                .addItemDecoration(RecyclerViewDivider.linearLayout(this, LinearLayoutManager.HORIZONTAL))
    }

    private fun initListItem() {
        mModuleAdapter
                .addItem(ListItem(getString(R.string.title_adapter_act), OnListItemClickListener { _, _, _ ->
                    startActivity(Intent(mContext, AdapterActivity::class.java))
                }))
                .addItem(ListItem(getString(R.string.title_capture_act), OnListItemClickListener { _, _, _ ->
                    startActivityForResult(Intent(mContext, CaptureActivity::class.java), SCAN_QRCODE_REQUEST_CODE)
                }))
                .addItem(ListItem(getString(R.string.title_swipe_back_act), OnListItemClickListener { _, _, _ ->
                    startActivity(Intent(mContext, SwipeBackActivity::class.java))
                })).addItem(ListItem(getString(R.string.title_carousel_view_act), OnListItemClickListener { _, _, _ ->
                    startActivity(Intent(mContext, CarouselViewActivity::class.java))
                })).addItem(ListItem(getString(R.string.title_eventbus_act), OnListItemClickListener { _, _, _ ->
                    startActivity(Intent(mContext, EventBusActivity::class.java))
                })).addItem(ListItem(getString(R.string.title_dialogs_act), OnListItemClickListener { _, _, _ ->
                    startActivity(Intent(mContext, DialogsActivity::class.java))
                })).addItem(ListItem(getString(R.string.title_bottom_tab_act), OnListItemClickListener { _, _, _ ->
                    startActivity(Intent(mContext, BottomTabActivity::class.java))
                }))
                .notifyDataSetChanged()
    }

    companion object {
        private const val SCAN_QRCODE_REQUEST_CODE = 0x1234
    }

}
