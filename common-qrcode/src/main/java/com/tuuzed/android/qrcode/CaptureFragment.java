package com.tuuzed.android.qrcode;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.tuuzed.android.qrcode.internal.StatusBarUtil;
import com.tuuzed.android.qrcode.internal.camera.CameraManager;
import com.tuuzed.android.util.LogUtil;

import java.io.IOException;


public class CaptureFragment extends Fragment implements SurfaceHolder.Callback {
    private static final String TAG = "CaptureFragment";
    private static final int DECODE_WHAT = 0x1000;

    public static CaptureFragment newInstance() {
        Bundle args = new Bundle();
        CaptureFragment fragment = new CaptureFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private SurfaceView mSvPreview;
    private RelativeLayout mRlContainer;
    private RelativeLayout mRlCropView;
    private CameraManager mCameraManager;
    @Nullable
    private DecodeThread mDecodeThread;
    private Rect mCropRect;
    private boolean isHasSurface = false;
    @Nullable
    private CaptureCallback mCallback;


    private final Handler mDecodeHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == DECODE_WHAT) {
                tryDecode((byte[]) msg.obj, msg.arg1, msg.arg2);
            }
        }
    };

    /**
     * 重新扫描
     */
    public void rescan() {
        if (mDecodeThread != null) {
            mDecodeThread.requestPreviewFrame();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.capture_frag, container, false);
        initView(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mCameraManager = new CameraManager(requireContext().getApplicationContext());
        if (isHasSurface) {
            initCameraAndStartDecode();
        } else {
            mSvPreview.getHolder().addCallback(this);
        }
    }

    @Override
    public void onPause() {
        if (mDecodeThread != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mDecodeThread.quitSafely();
            } else {
                mDecodeThread.quit();
            }
        }
        mCameraManager.closeDriver();
        if (!isHasSurface) mSvPreview.getHolder().removeCallback(this);
        super.onPause();
    }


    public void setCaptureCallback(@NonNull CaptureCallback callback) {
        this.mCallback = callback;
    }

    public boolean getTorchState() {
        return mCameraManager.getTorchState();
    }

    public void setTorchState(boolean newState) {
        mCameraManager.setTorch(newState);
    }

    private void initView(View view) {
        mSvPreview = view.findViewById(R.id.sv_preview);
        mRlContainer = view.findViewById(R.id.rl_container);
        mRlCropView = view.findViewById(R.id.rl_crop_view);
        ImageView ivScanLine = view.findViewById(R.id.iv_scan_line);
        TranslateAnimation animation = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT, .99f);
        animation.setDuration(4500);
        animation.setRepeatCount(-1);
        animation.setRepeatMode(Animation.RESTART);
        ivScanLine.startAnimation(animation);
    }

    private void initCameraAndStartDecode() {
        SurfaceHolder surfaceHolder = mSvPreview.getHolder();
        if (surfaceHolder == null) {
            throw new IllegalStateException("No SurfaceHolder provided");
        }
        if (mCameraManager.isOpen()) {
            LogUtil.w(TAG, "initCamera() while already open -- late SurfaceView callback?");
            return;
        }
        try {
            mCameraManager.openDriver(surfaceHolder);
            initCrop();
            mCameraManager.startPreview();
            mDecodeThread = new DecodeThread(mCameraManager, mDecodeHandler, DECODE_WHAT);
            mDecodeThread.start();
            mDecodeThread.requestPreviewFrame();
        } catch (IOException | RuntimeException e) {
            LogUtil.w(TAG, "initCameraAndStartDecode: ", e);
            if (mCallback != null) {
                mCallback.onCameraError(e);
            }
        }
    }

    @SuppressWarnings("SuspiciousNameCombination")
    private void initCrop() {
        int cameraWidth = mCameraManager.getCameraResolution().y;
        int cameraHeight = mCameraManager.getCameraResolution().x;
        /* 获取布局中扫描框的位置信息 */
        int[] location = new int[2];
        mRlCropView.getLocationInWindow(location);
        int cropLeft = location[0];
        int cropTop = location[1] - StatusBarUtil.getStatusBarHeight(requireContext());
        int cropWidth = mRlCropView.getWidth();
        int cropHeight = mRlCropView.getHeight();
        /* 获取布局容器的宽高 */
        int containerWidth = mRlContainer.getWidth();
        int containerHeight = mRlContainer.getHeight();
        /* 计算最终截取的矩形的左上角顶点x坐标 */
        int x = cropLeft * cameraWidth / containerWidth;
        /* 计算最终截取的矩形的左上角顶点y坐标 */
        int y = cropTop * cameraHeight / containerHeight;
        /* 计算最终截取的矩形的宽度 */
        int width = cropWidth * cameraWidth / containerWidth;
        /* 计算最终截取的矩形的高度 */
        int height = cropHeight * cameraHeight / containerHeight;

        /* 生成最终的截取的矩形 */
        mCropRect = new Rect(x, y, width + x, height + y);
    }

    private void tryDecode(final byte[] data, final int width, final int height) {
        final byte[] rotatedData = new byte[data.length];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                rotatedData[x * height + height - y - 1] = data[x + y * width];
            }
        }
        @SuppressWarnings("SuspiciousNameCombination")
        PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(rotatedData,
                height, // width
                width,  // height
                mCropRect.left, mCropRect.top,
                mCropRect.width(), mCropRect.height(),
                false);
        BinaryBitmap src = new BinaryBitmap(new HybridBinarizer(source));
        QRCodeUtil.decodeQRCode(src, result -> {
            if (result != null) {
                LogUtil.d(TAG, "onResult: " + result);
                if (mCallback != null) {
                    mCallback.onResult(result);
                }
            } else if (mDecodeThread != null) {
                mDecodeThread.requestPreviewFrame();
            }
        });
    }

    //===================== SurfaceHolder.Callback =====================//
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (holder == null) {
            LogUtil.w(TAG, "*** WARNING *** surfaceCreated() gave us a null surface!");
        }
        if (!isHasSurface) {
            isHasSurface = true;
            initCameraAndStartDecode();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        isHasSurface = false;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

}