package com.tuuzed.android.eventbus;

import android.support.annotation.IntDef;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Subscribe {

    @IntDef(flag = true, value = {EventBus.POSTING, EventBus.MAIN, EventBus.BACKGROUND, EventBus.ASYNC})
    @Retention(RetentionPolicy.SOURCE)
    @interface ThreadMode {
    }

    /**
     * 线程模式
     *
     * @return {@link ThreadMode}
     */

    @ThreadMode
    int threadMode() default EventBus.POSTING;

    /**
     * 粘性的
     *
     * @return
     */
    boolean sticky() default false;

    /**
     * 优先级
     * 数值越大优先级越高
     *
     * @return
     */
    int priority() default 0;
}
