package com.tuuzed.android.sqlite.internal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.tuuzed.android.sqlite.annotation.Table;

import java.util.List;

public class TableInfo {
    @NonNull
    public final String tableName;
    public final List<ColumnInfo> columnInfos;

    @Nullable
    static TableInfo create(@NonNull Class<?> tableClass) {
        Table table = tableClass.getAnnotation(Table.class);
        if (table == null) return null;
        String tableName = table.name();
        if (StringUtil.isEmpty(tableName)) {
            tableName = StringUtil.camelToUnderline(tableClass.getSimpleName());
        }
        return new TableInfo(tableName, tableClass);
    }

    private TableInfo(@NonNull String tableName, Class<?> tableClass) {
        this.tableName = tableName;
        this.columnInfos = ColumnInfos.getColumnInfos(tableClass);
    }
}
