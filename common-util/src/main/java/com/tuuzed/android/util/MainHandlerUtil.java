package com.tuuzed.android.util;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

public class MainHandlerUtil {

    private static class MainHandlerHolder {
        private static final Handler instance = new Handler(Looper.getMainLooper());
    }

    public static void runOnUiThread(@NonNull Runnable action) {
        runOnMainThread(action);
    }

    public static void runOnMainThread(@NonNull Runnable action) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            action.run();
        } else {
            MainHandlerHolder.instance.post(action);
        }
    }
}
