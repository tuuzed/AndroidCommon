package com.tuuzed.android.ui;

import android.support.annotation.IntDef;

@IntDef(flag = true, value = {
        DatePicker.TYPE_ALL,
        DatePicker.TYPE_YMDH,
        DatePicker.TYPE_YMD,
        DatePicker.TYPE_YM,
        DatePicker.TYPE_Y,
        DatePicker.TYPE_HM,
})
public @interface DatePickerType {
}
