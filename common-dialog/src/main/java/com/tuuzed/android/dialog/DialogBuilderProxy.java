package com.tuuzed.android.dialog;

import android.content.Context;
import android.support.annotation.ArrayRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import java.util.Arrays;
import java.util.List;

public class DialogBuilderProxy extends BaseDialog.Builder<DialogBuilderProxy, BaseDialog> {

    public static DialogBuilderProxy newProxy(@NonNull Context context) {
        return new DialogBuilderProxy(context);
    }

    private DialogBuilderProxy(@NonNull Context context) {
        super(context);
    }

    @NonNull
    @Override
    public BaseDialog<DialogBuilderProxy> create() {
        throw new IllegalStateException("no support");
    }

    @NonNull
    public InputDialog.Builder input() {
        InputDialog.Builder builder = new InputDialog.Builder(context);
        copyBuilder(builder);
        return builder;
    }

    @NonNull
    public DatePickerDialog.Builder datePicker() {
        DatePickerDialog.Builder builder = new DatePickerDialog.Builder(context);
        copyBuilder(builder);
        return builder;
    }

    @NonNull
    public DoubleDatePickerDialog.Builder doubleDatePicker() {
        DoubleDatePickerDialog.Builder builder = new DoubleDatePickerDialog.Builder(context);
        copyBuilder(builder);
        return builder;
    }

    @NonNull
    public MessageDialog.Builder message(@StringRes int resId) {
        return message(context.getString(resId));
    }

    @NonNull
    public MessageDialog.Builder message(String message) {
        MessageDialog.Builder builder = new MessageDialog.Builder(context);
        copyBuilder(builder);
        return builder.message(message);
    }

    @NonNull
    public ProgressBarDialog.Builder progressBar(@StringRes int resId) {
        return progressBar(context.getString(resId));
    }

    @NonNull
    public ProgressBarDialog.Builder progressBar(String message) {
        ProgressBarDialog.Builder builder = new ProgressBarDialog.Builder(context);
        copyBuilder(builder);
        return builder.message(message);
    }

    @NonNull
    public ListDialog.Builder items(@ArrayRes int resId) {
        return items(context.getResources().getStringArray(resId));
    }

    @NonNull
    public ListDialog.Builder items(String... items) {
        return items(Arrays.asList(items));
    }

    @NonNull
    public ListDialog.Builder items(List<String> items) {
        ListDialog.Builder builder = new ListDialog.Builder(context);
        copyBuilder(builder);
        return builder.items(items);
    }


    @NonNull
    public CustomViewDialog.Builder contentView(@LayoutRes int contentViewId) {
        CustomViewDialog.Builder builder = new CustomViewDialog.Builder(context);
        copyBuilder(builder);
        return builder.contentView(contentViewId);
    }

    private void copyBuilder(BaseDialog.Builder dest) {
        dest.titleBackgroundColor = titleBackgroundColor;
        dest.icon = icon;
        dest.title = title;
        dest.titleTextColor = titleTextColor;

        dest.negativeText = negativeText;
        dest.negativeTextColor = negativeTextColor;
        dest.negativeClickListener = negativeClickListener;

        dest.neutralText = neutralText;
        dest.neutralTextColor = neutralTextColor;
        dest.neutralClickListener = neutralClickListener;

        dest.positiveText = positiveText;
        dest.positiveTextColor = positiveTextColor;
        dest.positiveClickListener = positiveClickListener;

        dest.cancelable = cancelable;
        dest.showListener = showListener;
        dest.cancelListener = cancelListener;
        dest.dismissListener = dismissListener;
        dest.keyListener = keyListener;

    }

}
