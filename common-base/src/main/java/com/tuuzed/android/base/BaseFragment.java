package com.tuuzed.android.base;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {

    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public Context getContext() {
        if (mContext == null) return super.getContext();
        else return mContext;
    }
}
