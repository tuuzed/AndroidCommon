package com.tuuzed.android.sqlite;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Java数据类型SQLite数据类型映射
 */
public final class SQLiteType {
    private static final Map<Class<?>, String> types = new HashMap<>();

    private static final String INTEGER = "INTEGER";
    private static final String REAL = "REAL";
    private static final String TEXT = "TEXT";
    private static final String BLOB = "BLOB";

    static {
        types.put(String.class, SQLiteType.TEXT);

        types.put(Date.class, SQLiteType.INTEGER);

        types.put(boolean.class, SQLiteType.INTEGER);
        types.put(Boolean.class, SQLiteType.INTEGER);

        types.put(byte.class, SQLiteType.INTEGER);
        types.put(Byte.class, SQLiteType.INTEGER);

        types.put(char.class, SQLiteType.TEXT);
        types.put(Character.class, SQLiteType.TEXT);

        types.put(short.class, SQLiteType.INTEGER);
        types.put(Short.class, SQLiteType.INTEGER);

        types.put(int.class, SQLiteType.INTEGER);
        types.put(Integer.class, SQLiteType.INTEGER);

        types.put(long.class, SQLiteType.INTEGER);
        types.put(Long.class, SQLiteType.INTEGER);

        types.put(float.class, SQLiteType.REAL);
        types.put(Float.class, SQLiteType.REAL);

        types.put(double.class, SQLiteType.REAL);
        types.put(Double.class, SQLiteType.REAL);

        types.put(byte[].class, SQLiteType.BLOB);
        types.put(Byte[].class, SQLiteType.BLOB);
    }


    @Nullable
    public static String getType(@NonNull Class<?> clazz) {
        return types.get(clazz);
    }
}
