package com.tuuzed.android.sample.domain;

import com.tuuzed.android.adapter.ItemViewHolder;

public interface OnListItemClickListener {
    void onListItemClick(ItemViewHolder holder, ListItem listItem, int position);
}
