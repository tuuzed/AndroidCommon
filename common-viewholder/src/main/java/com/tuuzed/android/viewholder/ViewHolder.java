package com.tuuzed.android.viewholder;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;

public interface ViewHolder {

    interface Action {
        void apply(View view, int index);
    }

    <T extends View> T find(@IdRes int id);

    <T extends View> T find(@IdRes int id, @NonNull Class<T> type);

    CharSequence text(@IdRes int id);

    void text(@IdRes int id, @NonNull CharSequence text);

    void text(@IdRes int id, @StringRes int resId);

    void image(@IdRes int id, @DrawableRes int resId);

    void image(@IdRes int id, @NonNull Drawable drawable);

    void image(@IdRes int id, @NonNull Bitmap bitmap);

    void apply(@IdRes int id, @NonNull Action action);

    void apply(@IdRes int[] ids, @NonNull Action action);

    void click(@IdRes int id, @NonNull View.OnClickListener listener);

    void click(@IdRes int[] ids, @NonNull View.OnClickListener listener);

    void longClick(@IdRes int id, @NonNull View.OnLongClickListener listener);

    void longClick(@IdRes int[] ids, @NonNull View.OnLongClickListener listener);

    void clear();
}