package com.tuuzed.android.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tuuzed.android.ui.DatePicker;
import com.tuuzed.android.ui.DatePickerType;

import java.util.Date;

public class DatePickerDialog extends BaseDialog<DatePickerDialog.Builder> {

    private DatePicker mDatePicker;
    @Nullable
    private DatePicker.OnDateChangedListener onDateChangedListener = null;
    @Nullable
    private Callback callback = null;


    public static DatePickerDialog newInstance(@NonNull Builder params) {
        DatePickerDialog fragment = new DatePickerDialog();
        fragment.setParams(params);
        fragment.onDateChangedListener = params.onDateChangedListener;
        fragment.callback = params.callback;
        return fragment;
    }

    @Nullable
    @Override
    protected View onContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                                  @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.date_picker_layout, container, false);
        initView(view);
        return view;
    }

    @Override
    protected View.OnClickListener getPositiveClickListener() {
        return v -> {
            if (callback != null) {
                callback.call(mDatePicker.getDate());
            }
            if (mParams.positiveClickListener != null) {
                mParams.positiveClickListener.onClick(getDialog(), DialogInterface.BUTTON_POSITIVE);
            }
        };
    }

    private void initView(View view) {
        mDatePicker = view.findViewById(R.id.date_picker);

        // intDatePicker
        mDatePicker.setMinYear(mParams.minYear);
        mDatePicker.setMaxYear(mParams.maxYear);
        mDatePicker.setType(mParams.datePickerType);
        mDatePicker.setDate(mParams.date);
        mDatePicker.setOnDateChangedListener(newDate -> {
            if (onDateChangedListener != null) {
                onDateChangedListener.onDateChanged(newDate);
            }
        });
    }

    public static class Builder extends BaseDialog.Builder<Builder, DatePickerDialog> {
        private Date date = null;
        private int datePickerType = -1;
        private int minYear = -1;
        private int maxYear = -1;
        private DatePicker.OnDateChangedListener onDateChangedListener = null;
        private Callback callback = null;

        Builder(@NonNull Context context) {
            super(context);
        }

        @NonNull
        @Override
        public DatePickerDialog create() {
            if (date == null) date = new Date();
            if (datePickerType < 0) datePickerType = DatePicker.TYPE_ALL;
            if (maxYear < 0) maxYear = 2100;
            if (minYear < 0) minYear = 1900;
            return DatePickerDialog.newInstance(this);
        }

        public Builder date(Date date) {
            this.date = date;
            return this;
        }

        public Builder datePickerType(@DatePickerType int datePickerType) {
            this.datePickerType = datePickerType;
            return this;
        }

        public Builder rangeYear(@IntRange(from = 1900, to = 2100) int minYear,
                                 @IntRange(from = 1900, to = 2100) int maxYear) {
            this.minYear = minYear;
            this.maxYear = maxYear;
            return this;
        }

        public Builder listener(DatePicker.OnDateChangedListener listener) {
            this.onDateChangedListener = listener;
            return this;
        }

        public Builder callback(Callback callback) {
            this.callback = callback;
            return this;
        }
    }

    public interface Callback {
        void call(@NonNull Date date);
    }
}
