package com.tuuzed.android.sqlite.internal;

public class StringUtil {


    private StringUtil() {
    }


    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    public static boolean isNotEmpty(CharSequence str) {
        return str != null && str.length() > 0;
    }

    /**
     * 首字母转小写
     *
     * @param str 需要转换的字符串
     * @return 转换好的字符串
     */
    public static String toLowerCaseFirstOne(String str) {
        if (isEmpty(str)) {
            return "";
        }
        if (Character.isLowerCase(str.charAt(0))) {
            return str;
        } else {
            return (new StringBuilder())
                    .append(Character.toLowerCase(str.charAt(0)))
                    .append(str.substring(1)).toString();
        }
    }

    /**
     * 首字母转大写
     *
     * @param str 需要转换的字符串
     * @return 转换好的字符串
     */
    public static String toUpperCaseFirstOne(String str) {
        if (isEmpty(str)) {
            return "";
        }
        if (Character.isUpperCase(str.charAt(0))) {
            return str;
        } else {
            return (new StringBuilder())
                    .append(Character.toUpperCase(str.charAt(0)))
                    .append(str.substring(1)
                    ).toString();
        }
    }

    /**
     * 驼峰命名转下划线
     *
     * @param str 需要转换的字符串
     * @return 转换好的字符串
     */
    public static String camelToUnderline(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        str = toLowerCaseFirstOne(str);
        int len = str.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = str.charAt(i);
            if (Character.isUpperCase(c)) {
                sb.append('_');
                sb.append(Character.toLowerCase(c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * 下划线转驼峰
     *
     * @param str 需要转换的字符串
     * @return 转换好的字符串
     */
    public static String underlineToCamel(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        str = toUpperCaseFirstOne(str);
        int len = str.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = str.charAt(i);
            if (c == '_') {
                if (++i < len) {
                    sb.append(Character.toUpperCase(str.charAt(i)));
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

}
