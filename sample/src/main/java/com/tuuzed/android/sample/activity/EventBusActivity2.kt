package com.tuuzed.android.sample.activity

import android.os.Bundle
import com.tuuzed.android.base.BaseActivity
import com.tuuzed.android.eventbus.EventBus
import com.tuuzed.android.eventbus.Subscribe
import com.tuuzed.android.sample.R
import com.tuuzed.android.utilkt.toastShort

class EventBusActivity2 : BaseActivity() {
    override fun isHomeAsUpEnabled(): Boolean = true
    override fun isSwipeBackEnable(): Boolean = true
    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        setContentView(R.layout.eventbus_act2)
        setSupportActionBar(findViewById(R.id.toolbar))
        EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = EventBus.MAIN, sticky = true)
    fun onStickyEvent(event: EventBusActivity.StickyEvent) {
        toastShort(event.toString())
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }
}
