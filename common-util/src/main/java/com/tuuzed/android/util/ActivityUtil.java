package com.tuuzed.android.util;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;

public final class ActivityUtil {

    public static void replaceFragmentTo(@NonNull android.app.FragmentManager fragmentManager,
                                         @IdRes int containerViewId,
                                         @NonNull android.app.Fragment fragment) {
        replaceFragmentTo(fragmentManager, containerViewId, fragment, fragment.getClass().getSimpleName());
    }

    public static void replaceFragmentTo(@NonNull android.app.FragmentManager fragmentManager,
                                         @IdRes int containerViewId,
                                         @NonNull android.app.Fragment fragment,
                                         @NonNull String tag) {
        fragmentManager.beginTransaction()
                .replace(containerViewId, fragment, tag)
                .commit();
    }


    public static void replaceSupportFragmentTo(@NonNull android.support.v4.app.FragmentManager fragmentManager,
                                                @IdRes int containerViewId,
                                                @NonNull android.support.v4.app.Fragment fragment) {
        replaceSupportFragmentTo(fragmentManager, containerViewId, fragment, fragment.getClass().getSimpleName());
    }

    public static void replaceSupportFragmentTo(@NonNull android.support.v4.app.FragmentManager fragmentManager,
                                                @IdRes int containerViewId,
                                                @NonNull android.support.v4.app.Fragment fragment,
                                                @NonNull String tag) {
        fragmentManager.beginTransaction()
                .replace(containerViewId, fragment, tag)
                .commit();
    }


}
