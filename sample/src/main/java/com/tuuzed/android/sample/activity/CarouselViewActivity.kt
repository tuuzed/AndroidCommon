package com.tuuzed.android.sample.activity

import android.os.Bundle
import com.tuuzed.android.base.BaseActivity
import com.tuuzed.android.sample.R
import kotlinx.android.synthetic.main.carousel_view_act.*

class CarouselViewActivity : BaseActivity() {
    override fun isHomeAsUpEnabled(): Boolean = true
    override fun isSwipeBackEnable(): Boolean = true
    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
        setContentView(R.layout.carousel_view_act)
        setSupportActionBar(findViewById(R.id.toolbar))
        carousel_view.setImagesRes(intArrayOf(R.mipmap.cv_1, R.mipmap.cv_2))
    }
}