package com.tuuzed.android.sqlite.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Database {

    int version() default 1;

    String name() default "";

    boolean isDefault() default false;

    boolean echoSQL() default false;

    Class<?>[] tables();
}
