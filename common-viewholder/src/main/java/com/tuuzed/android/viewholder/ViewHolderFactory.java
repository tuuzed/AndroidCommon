package com.tuuzed.android.viewholder;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.view.View;

public final class ViewHolderFactory {
    private ViewHolderFactory() {
    }

    @NonNull
    public static ViewHolder of(@NonNull Activity target) {
        return new ActivityViewHolder(target);
    }

    @NonNull
    public static ViewHolder of(@NonNull View target) {
        return new ViewViewHolder(target);
    }

    @NonNull
    public static ViewHolder of(@NonNull Dialog target) {
        return new DialogViewHolder(target);
    }
}
