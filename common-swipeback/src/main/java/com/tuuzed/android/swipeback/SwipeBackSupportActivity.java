package com.tuuzed.android.swipeback;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public abstract class SwipeBackSupportActivity extends AppCompatActivity {
    @Nullable
    private SwipeBackHelper mHelper;

    public boolean isSwipeBackEnable() {
        return false;
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (isSwipeBackEnable()) {
            mHelper = new SwipeBackHelper(this);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mHelper != null) {
            mHelper.onPostCreate();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mHelper != null) {
            mHelper.setBackEnable(isSwipeBackEnable());
        }
    }

    @Override
    public void onBackPressed() {
        if (isSwipeBackEnable()) {
            if (!getFragmentManager().popBackStackImmediate()) {
                scrollToFinishActivity();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        if (mHelper != null) {
            mHelper.onActivityDestroy();
        }
        super.onDestroy();
    }


    @Override
    public <T extends View> T findViewById(int id) {
        if (mHelper != null) {
            T v = super.findViewById(id);
            if (v == null && mHelper != null)
                return mHelper.findViewById(id);
            return v;
        }
        return super.findViewById(id);
    }


    @Nullable
    public SwipeBackLayout getSwipeBackLayout() {
        if (mHelper == null) {
            return null;
        }
        return mHelper.getBackLayout();
    }

    public void scrollToFinishActivity() {
        if (mHelper != null) {
            mHelper.scrollToFinishActivity();
        }
    }

}