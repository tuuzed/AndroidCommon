package com.tuuzed.android.viewholder;

import android.view.View;

public class ViewViewHolder extends AbstractViewHolder {
    private View mParent;

    ViewViewHolder(View target) {
        super(16);
        this.mParent = target;
    }

    @Override
    <T extends View> T findViewById(int id) {
        return mParent.findViewById(id);
    }

    @Override
    public void clear() {
        super.clear();
        mParent = null;
    }
}
