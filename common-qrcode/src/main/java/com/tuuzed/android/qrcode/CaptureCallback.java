package com.tuuzed.android.qrcode;

import android.support.annotation.NonNull;

import com.google.zxing.Result;

public interface CaptureCallback {
    void onResult(@NonNull Result result);

    void onCameraError(@NonNull Throwable t);
}
