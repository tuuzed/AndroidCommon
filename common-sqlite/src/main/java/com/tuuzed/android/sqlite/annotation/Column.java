package com.tuuzed.android.sqlite.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用于标记列
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {
    /**
     * 列名
     *
     * @return 列名
     */
    String name() default "";

    /**
     * 是否可为空
     *
     * @return 是否可为空
     */
    boolean nullable() default true;
}