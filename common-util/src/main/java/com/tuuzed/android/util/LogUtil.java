package com.tuuzed.android.util;

import android.annotation.SuppressLint;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SuppressWarnings("WeakerAccess")
@SuppressLint("ShiftFlags")
public final class LogUtil {
    private static volatile int sLogLevel = 0;
    public static final int VERBOSE = 2;
    public static final int DEBUG = 3;
    public static final int INFO = 4;
    public static final int WARN = 5;
    public static final int ERROR = 6;
    public static final int OFF = 100;

    private static final int TAG_LENGTH = 23;

    @IntDef(flag = true, value = {VERBOSE, DEBUG, INFO, WARN, ERROR, OFF})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LogLevel {
    }

    public static void setLogLevel(@LogLevel int logLevel) {
        sLogLevel = logLevel;
    }

    public static int v(@NonNull String tag, @NonNull String msg) {
        if (sLogLevel <= VERBOSE) {
            if (tag.length() > TAG_LENGTH) tag = tag.substring(0, TAG_LENGTH);
            return Log.v(tag, msg);
        }
        return 0;
    }

    public static int v(@NonNull String tag, @NonNull String msg, @Nullable Throwable tr) {
        if (sLogLevel <= VERBOSE) {
            if (tag.length() > TAG_LENGTH) tag = tag.substring(0, TAG_LENGTH);
            return Log.v(tag, msg, tr);
        }
        return 0;
    }

    public static int d(@NonNull String tag, @NonNull String msg) {
        if (sLogLevel <= DEBUG) {
            if (tag.length() > TAG_LENGTH) tag = tag.substring(0, TAG_LENGTH);
            return Log.d(tag, msg);
        }
        return 0;
    }

    public static int d(@NonNull String tag, @NonNull String msg, @Nullable Throwable tr) {
        if (sLogLevel <= DEBUG) {
            if (tag.length() > TAG_LENGTH) tag = tag.substring(0, TAG_LENGTH);
            return Log.d(tag, msg, tr);
        }
        return 0;
    }

    public static int i(@NonNull String tag, @NonNull String msg) {
        if (sLogLevel <= INFO) {
            if (tag.length() > TAG_LENGTH) tag = tag.substring(0, TAG_LENGTH);
            return Log.i(tag, msg);
        }
        return 0;
    }

    public static int i(@NonNull String tag, @NonNull String msg, @Nullable Throwable tr) {
        if (sLogLevel <= INFO) {
            if (tag.length() > TAG_LENGTH) tag = tag.substring(0, TAG_LENGTH);
            return Log.i(tag, msg, tr);
        }
        return 0;
    }

    public static int w(@NonNull String tag, @NonNull String msg) {
        if (sLogLevel <= WARN) {
            if (tag.length() > TAG_LENGTH) tag = tag.substring(0, TAG_LENGTH);
            return Log.w(tag, msg);
        }
        return 0;
    }

    public static int w(@NonNull String tag, @NonNull String msg, @Nullable Throwable tr) {
        if (sLogLevel <= WARN) {
            if (tag.length() > TAG_LENGTH) tag = tag.substring(0, TAG_LENGTH);
            return Log.w(tag, msg, tr);
        }
        return 0;
    }

    public static int w(@NonNull String tag, @Nullable Throwable tr) {
        if (sLogLevel <= WARN) {
            if (tag.length() > TAG_LENGTH) tag = tag.substring(0, TAG_LENGTH);
            return Log.w(tag, tr);
        }
        return 0;
    }

    public static int e(@NonNull String tag, @NonNull String msg) {
        if (sLogLevel <= ERROR) {
            if (tag.length() > TAG_LENGTH) tag = tag.substring(0, TAG_LENGTH);
            return Log.e(tag, msg);
        }
        return 0;
    }

    public static int e(@NonNull String tag, @NonNull String msg, @Nullable Throwable tr) {
        if (sLogLevel <= ERROR) {
            if (tag.length() > TAG_LENGTH) tag = tag.substring(0, TAG_LENGTH);
            return Log.e(tag, msg, tr);
        }
        return 0;
    }
}
