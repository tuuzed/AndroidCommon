package com.tuuzed.android.sqlite;

/**
 * 数据库升级回调
 */
public interface UpgradeCallback {
    String getUpgradeSQL(int oldVersion, int newVersion);
}
