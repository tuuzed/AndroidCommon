package com.tuuzed.android.base;

import android.view.MenuItem;

import com.tuuzed.android.swipeback.SwipeBackSupportActivity;

public abstract class BaseActivity extends SwipeBackSupportActivity {

    @Override
    public boolean isSwipeBackEnable() {
        return false;
    }

    public boolean isHomeAsUpEnabled() {
        return false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isHomeAsUpEnabled()) {
            android.app.ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
            android.support.v7.app.ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null && item.getItemId() == android.R.id.home && isHomeAsUpEnabled()) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
