package com.tuuzed.android.sample

import android.app.Application
import com.squareup.leakcanary.LeakCanary
import com.tuuzed.android.util.LogUtil
import com.tuuzed.android.utilkt.*

class CommonApp : Application() {

    override fun onCreate() {
        super.onCreate()
        testLog()
        if (!LeakCanary.isInAnalyzerProcess(this)) {
            LeakCanary.install(this)
        }
    }

    private fun testLog() {
        LogUtil.setLogLevel(LogUtil.DEBUG)
        LOGV("CommonApp")
        LOGD("CommonApp")
        LOGI("CommonApp")
        LOGW("CommonApp")
        LOGE("CommonApp")
    }

}
