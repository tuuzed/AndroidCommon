package com.tuuzed.android.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public interface ItemViewBinder<T, VH extends RecyclerView.ViewHolder> {
    @NonNull
    VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType);

    void onBindViewHolder(@NonNull VH holder, @NonNull T item, int position);

}
