package com.tuuzed.android.sqlite.args;


public class FindArgs<T> {
    public final Class<T> tableClass;
    private boolean distinct;
    private String[] columns;
    private String whereClause;
    private String[] whereArgs;
    private String groupBy;
    private String having;
    private String orderBy;
    private String limit;

    public static <T> FindArgs<T> of(Class<T> tableClass) {
        return new FindArgs<>(tableClass);
    }

    private FindArgs(Class<T> tableClass) {
        this.tableClass = tableClass;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public String[] getColumns() {
        return columns;
    }

    public String getWhereClause() {
        return whereClause;
    }

    public String[] getWhereArgs() {
        return whereArgs;
    }

    public String getGroupBy() {
        return groupBy;
    }

    public String getHaving() {
        return having;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public String getLimit() {
        return limit;
    }

    public FindArgs<T> distinct(boolean distinct) {
        this.distinct = distinct;
        return this;
    }

    public FindArgs<T> columns(String[] columns) {
        this.columns = columns;
        return this;
    }

    public FindArgs<T> where(String whereClause, String[] whereArgs) {
        this.whereClause = whereClause;
        this.whereArgs = whereArgs;
        return this;
    }

    public FindArgs<T> groupBy(String groupBy, String having) {
        this.groupBy = groupBy;
        this.having = having;
        return this;
    }

    public FindArgs<T> orderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public FindArgs<T> limit(String limit) {
        this.limit = limit;
        return this;
    }
}
