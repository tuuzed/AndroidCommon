package com.tuuzed.android.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import java.util.Set;

public final class AppSharedPreferenceUtil {

    private static SharedPreferences getAppSharedPreferences(@NonNull Context context) {
        return context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE);
    }

    public static boolean getAppSpItem(@NonNull Context context, @NonNull String key, boolean defValue) {
        return getAppSharedPreferences(context).getBoolean(key, defValue);
    }

    public static int getAppSpItem(@NonNull Context context, @NonNull String key, int defValue) {
        return getAppSharedPreferences(context).getInt(key, defValue);
    }

    public static float getAppSpItem(@NonNull Context context, @NonNull String key, float defValue) {
        return getAppSharedPreferences(context).getFloat(key, defValue);
    }

    public static float getAppSpItem(@NonNull Context context, @NonNull String key, long defValue) {
        return getAppSharedPreferences(context).getLong(key, defValue);
    }

    public static String getAppSpItem(@NonNull Context context, @NonNull String key, String defValue) {
        return getAppSharedPreferences(context).getString(key, defValue);
    }

    public static Set<String> getAppSpItem(@NonNull Context context, @NonNull String key, Set<String> defValue) {
        return getAppSharedPreferences(context).getStringSet(key, defValue);
    }

    public static void setAppSpItem(@NonNull Context context, @NonNull String key, boolean value) {
        getAppSharedPreferences(context).edit().putBoolean(key, value).apply();
    }

    public static void setAppSpItem(@NonNull Context context, @NonNull String key, int value) {
        getAppSharedPreferences(context).edit().putInt(key, value).apply();
    }

    public static void setAppSpItem(@NonNull Context context, @NonNull String key, float value) {
        getAppSharedPreferences(context).edit().putFloat(key, value).apply();
    }

    public static void setAppSpItem(@NonNull Context context, @NonNull String key, long value) {
        getAppSharedPreferences(context).edit().putLong(key, value).apply();
    }

    public static void setAppSpItem(@NonNull Context context, @NonNull String key, String value) {
        getAppSharedPreferences(context).edit().putString(key, value).apply();
    }

    public static void setAppSpItem(@NonNull Context context, @NonNull String key, Set<String> value) {
        getAppSharedPreferences(context).edit().putStringSet(key, value).apply();
    }

}
