package com.tuuzed.android.qrcode;

import android.support.annotation.Nullable;

import com.google.zxing.Result;

public interface OnResultCallback {
    void onResult(@Nullable Result result);
}