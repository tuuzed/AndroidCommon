package com.tuuzed.android.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CustomViewDialog extends BaseDialog<CustomViewDialog.Builder> {

    public static CustomViewDialog newInstance(@NonNull Builder params) {
        CustomViewDialog fragment = new CustomViewDialog();
        fragment.setParams(params);
        return fragment;
    }

    @Nullable
    @Override
    protected View onContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                                  @Nullable Bundle savedInstanceState) {
        return inflater.inflate(mParams.contentViewId, container, false);
    }

    public static class Builder extends BaseDialog.Builder<Builder, CustomViewDialog> {
        @LayoutRes
        private int contentViewId;

        Builder(@NonNull Context context) {
            super(context);
        }

        @NonNull
        @Override
        public CustomViewDialog create() {
            return CustomViewDialog.newInstance(this);
        }

        public Builder contentView(@LayoutRes int contentViewId) {
            this.contentViewId = contentViewId;
            return this;
        }
    }
}
