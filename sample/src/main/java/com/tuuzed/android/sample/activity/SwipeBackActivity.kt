package com.tuuzed.android.sample.activity

import android.os.Bundle
import com.tuuzed.android.base.BaseActivity
import com.tuuzed.android.sample.R
import com.tuuzed.android.viewholder.ViewHolder
import com.tuuzed.android.viewholder.ViewHolderFactory

class SwipeBackActivity : BaseActivity() {

    lateinit var mViewHolder: ViewHolder

    override fun isHomeAsUpEnabled(): Boolean = true
    override fun isSwipeBackEnable(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.swipe_back_act)
        mViewHolder = ViewHolderFactory.of(this)
        setSupportActionBar(mViewHolder.find(R.id.toolbar))
    }


}
