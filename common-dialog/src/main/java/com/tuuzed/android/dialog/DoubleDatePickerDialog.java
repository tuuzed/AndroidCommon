package com.tuuzed.android.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tuuzed.android.ui.DatePicker;
import com.tuuzed.android.ui.DatePickerType;
import com.tuuzed.android.util.ToastUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DoubleDatePickerDialog extends BaseDialog<DoubleDatePickerDialog.Builder> {
    private RadioGroup mRgBeginAndEnd;
    private RadioButton mRbBegin;
    private RadioButton mRbEnd;
    private DatePicker mDatePicker;
    @Nullable
    private Callback callback = null;


    public static DoubleDatePickerDialog newInstance(@NonNull Builder params) {
        DoubleDatePickerDialog fragment = new DoubleDatePickerDialog();
        fragment.setParams(params);
        fragment.callback = params.callback;
        return fragment;
    }

    @Nullable
    @Override
    protected View onContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                                  @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.double_date_picker_layout, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mRgBeginAndEnd = view.findViewById(R.id.rg_begin_and_end);
        mRbBegin = view.findViewById(R.id.rb_begin);
        mRbEnd = view.findViewById(R.id.rb_end);
        mDatePicker = view.findViewById(R.id.date_picker);

        // Begin and End
        mRgBeginAndEnd.check(R.id.rb_begin);
        mRgBeginAndEnd.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.rb_begin) {
                mDatePicker.setDate(mParams.beginDate);
            } else if (checkedId == R.id.rb_end) {
                mDatePicker.setDate(mParams.endDate);
            }
        });
        final DateFormat dateFormat = getDateFormat(mParams.datePickerType);
        mRbBegin.setText("开始于\n" + dateFormat.format(mParams.beginDate));
        mRbEnd.setText("结束于\n" + dateFormat.format(mParams.endDate));
        // date picker
        mDatePicker.setMinYear(mParams.minYear);
        mDatePicker.setMaxYear(mParams.maxYear);
        mDatePicker.setDate(mParams.beginDate);
        mDatePicker.setType(mParams.datePickerType);
        mDatePicker.setOnDateChangedListener(newDate -> {
            if (mRgBeginAndEnd.getCheckedRadioButtonId() == R.id.rb_begin) {
                mParams.beginDate = newDate;
                mRbBegin.setText("开始于\n" + dateFormat.format(newDate));
            } else if (mRgBeginAndEnd.getCheckedRadioButtonId() == R.id.rb_end) {
                mParams.endDate = newDate;
                mRbEnd.setText("结束于\n" + dateFormat.format(newDate));
            }
        });
    }

    @Override
    protected View.OnClickListener getPositiveClickListener() {
        return v -> {
            if (callback != null) {
                if (mParams.beginDate.getTime() > mParams.endDate.getTime()) {
                    ToastUtil.showShort(requireContext(), "开始时间大于结束时间");
                    return;
                }
                callback.call(mParams.beginDate, mParams.endDate);
            }
            if (mParams.positiveClickListener != null) {
                mParams.positiveClickListener.onClick(getDialog(), DialogInterface.BUTTON_POSITIVE);
            }
        };
    }

    @NonNull
    private DateFormat getDateFormat(@DatePickerType int datePickerType) {
        switch (datePickerType) {
            case DatePicker.TYPE_ALL:
                return new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA);
            case DatePicker.TYPE_YMDH:
                return new SimpleDateFormat("yyyy-MM-dd HH", Locale.CHINA);
            case DatePicker.TYPE_YMD:
                return new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
            case DatePicker.TYPE_YM:
                return new SimpleDateFormat("yyyy-MM", Locale.CHINA);
            case DatePicker.TYPE_Y:
                return new SimpleDateFormat("yyyy", Locale.CHINA);
            case DatePicker.TYPE_HM:
                return new SimpleDateFormat("HH:mm", Locale.CHINA);
            default:
                return new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.CHINA);
        }
    }


    public static class Builder extends BaseDialog.Builder<Builder, DoubleDatePickerDialog> {
        private Date beginDate;
        private Date endDate;
        private int datePickerType = DatePicker.TYPE_ALL;
        private int minYear = -1;
        private int maxYear = -1;
        private Callback callback = null;

        Builder(@NonNull Context context) {
            super(context);
        }

        @NonNull
        public DoubleDatePickerDialog create() {
            if (beginDate == null) beginDate = new Date();
            if (endDate == null) endDate = new Date();
            if (maxYear < 0) maxYear = 2100;
            if (minYear < 0) minYear = 1900;
            return DoubleDatePickerDialog.newInstance(this);
        }

        public Builder beginDate(Date beginDate) {
            this.beginDate = beginDate;
            return this;
        }

        public Builder endDate(Date endDate) {
            this.endDate = endDate;
            return this;
        }

        public Builder datePickerType(@DatePickerType int datePickerType) {
            this.datePickerType = datePickerType;
            return this;
        }

        public Builder rangeYear(@IntRange(from = 1900, to = 2100) int minYear,
                                 @IntRange(from = 1900, to = 2100) int maxYear) {
            this.minYear = minYear;
            this.maxYear = maxYear;
            return this;
        }

        public Builder callback(Callback callback) {
            this.callback = callback;
            return this;
        }
    }

    public  interface Callback {
        void call(@NonNull Date beginDate, @NonNull Date endDate);

    }
}
