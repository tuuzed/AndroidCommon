package com.tuuzed.android.sqlite.internal;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Date;

public class SQLiteValueConverter {
    private static final String TAG = "SQLiteValueConverter";


    private SQLiteValueConverter() {
    }

    public static <T> void putToContentValues(@NonNull ContentValues values,
                                              @NonNull ColumnInfo columnInfo,
                                              @NonNull T t) {
        Object value = null;
        try {
            value = columnInfo.getValue(t);
        } catch (IllegalAccessException e) {
            Log.w(TAG, "putContentValue: ", e);
        }
        if (value != null) {
            putToContentValues(values, columnInfo.columnName, value);
        }
    }

    public static void putToContentValues(@NonNull ContentValues values,
                                          @NonNull String columnName,
                                          @NonNull Object value) {
        Class<?> type = value.getClass();
        if (value instanceof String) {
            values.put(columnName, (String) value);
        } else if (value instanceof Date) {
            values.put(columnName, ((Date) value).getTime());
        } else if (type == boolean.class || value instanceof Boolean) {
            values.put(columnName, (boolean) value ? 1 : 0);
        } else if (type == byte.class || value instanceof Byte) {
            values.put(columnName, (byte) value);
        } else if (type == char.class || value instanceof Character) {
            values.put(columnName, String.valueOf((char) value));
        } else if (type == short.class || value instanceof Short) {
            values.put(columnName, (short) value);
        } else if (type == int.class || value instanceof Integer) {
            values.put(columnName, (int) value);
        } else if (type == long.class || value instanceof Long) {
            values.put(columnName, (long) value);
        } else if (type == float.class || value instanceof Float) {
            values.put(columnName, (float) value);
        } else if (type == double.class || value instanceof Double) {
            values.put(columnName, (double) value);
        } else if (type == byte[].class || value instanceof byte[]) {
            values.put(columnName, (byte[]) value);
        }
    }

    public static <T> void setFieldValue(ColumnInfo columnInfo, T t, Cursor cursor)
            throws IllegalAccessException, InstantiationException {
        Class<?> type = columnInfo.javaType;
        int columnIndex = cursor.getColumnIndex(columnInfo.columnName);
        if (columnIndex == -1) return;
        Object value = null;
        if (type == String.class) {
            value = cursor.getString(columnIndex);
        } else if (type == Date.class) {
            value = new Date(cursor.getLong(columnIndex));
        } else if (type == boolean.class || type == Boolean.class) {
            value = cursor.getInt(columnIndex) == 1;
        } else if (type == byte.class || type == Byte.class) {
            value = (byte) cursor.getInt(columnIndex);
        } else if (type == char.class || type == Character.class) {
            value = cursor.getString(columnIndex).charAt(0);
        } else if (type == short.class || type == Short.class) {
            value = cursor.getShort(columnIndex);
        } else if (type == int.class || type == Integer.class) {
            value = cursor.getInt(columnIndex);
        } else if (type == long.class || type == Long.class) {
            value = cursor.getLong(columnIndex);
        } else if (type == float.class || type == Float.class) {
            value = cursor.getFloat(columnIndex);
        } else if (type == double.class || type == Double.class) {
            value = cursor.getDouble(columnIndex);
        }
        columnInfo.setValue(t, value);
    }

}
