package com.tuuzed.android.sqlite.args;

public class CountArgs<T> {
    public final Class<T> tableClass;
    private boolean distinct;
    private String whereClause;
    private String[] whereArgs;

    public static <T> CountArgs<T> of(Class<T> tableClass) {
        return new CountArgs<>(tableClass);
    }

    private CountArgs(Class<T> tableClass) {
        this.tableClass = tableClass;
    }


    public String getWhereClause() {
        return whereClause;
    }

    public String[] getWhereArgs() {
        return whereArgs;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public CountArgs<T> distinct(boolean distinct) {
        this.distinct = distinct;
        return this;
    }

    public CountArgs<T> where(String whereClause, String[] whereArgs) {
        this.whereClause = whereClause;
        this.whereArgs = whereArgs;
        return this;
    }
}
