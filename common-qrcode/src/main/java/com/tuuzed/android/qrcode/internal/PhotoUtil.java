package com.tuuzed.android.qrcode.internal;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

public class PhotoUtil {
    public static void choicePic(@NonNull Activity host,
                                 @NonNull CharSequence chooserTitle, int requestCode) {
        host.startActivityForResult(getIntent(chooserTitle), requestCode);
    }

    public static void choicePic(@NonNull android.app.Fragment host,
                                 @NonNull CharSequence chooserTitle, int requestCode) {
        host.startActivityForResult(getIntent(chooserTitle), requestCode);

    }

    public static void choicePic(@NonNull android.support.v4.app.Fragment host,
                                 @NonNull CharSequence chooserTitle, int requestCode) {
        host.startActivityForResult(getIntent(chooserTitle), requestCode);
    }

    private static Intent getIntent(@NonNull CharSequence chooserTitle) {
        Intent innerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        innerIntent.setType("image/*");
        return Intent.createChooser(innerIntent, chooserTitle);
    }
}
