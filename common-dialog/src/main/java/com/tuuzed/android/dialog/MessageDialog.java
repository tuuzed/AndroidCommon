package com.tuuzed.android.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MessageDialog extends BaseDialog<MessageDialog.Builder> {

    private TextView mTvMessage;

    public static MessageDialog newInstance(@NonNull Builder params) {
        MessageDialog fragment = new MessageDialog();
        fragment.setParams(params);
        return fragment;
    }

    @Nullable
    @Override
    protected View onContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                                 @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.message_layout, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mTvMessage = view.findViewById(R.id.tv_message);

        mTvMessage.setText(mParams.message);
        if (mParams.textColor != -1) {
            mTvMessage.setTextColor(mParams.textColor);
        }
    }

    public static class Builder extends BaseDialog.Builder<Builder,MessageDialog> {
        private String message;
        private int textColor = -1;

        Builder(@NonNull Context context) {
            super(context);
        }

        @NonNull
        @Override
        public MessageDialog create() {
            if (message == null) message = "";
            return MessageDialog.newInstance(this);
        }

        public Builder message(@StringRes int resId) {
            return message(context.getString(resId));
        }

        public Builder message(String message) {
            return message(message, -1);
        }

        public Builder message(@StringRes int resId, @ColorInt int textColor) {
            return message(context.getString(resId), textColor);
        }

        public Builder message(String message, @ColorInt int textColor) {
            this.message = message;
            this.textColor = textColor;
            return this;
        }
    }
}
