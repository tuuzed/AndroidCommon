package com.tuuzed.android.sqlite.internal;

import android.support.annotation.NonNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ColumnInfos {

    private static Map<Class<?>, List<ColumnInfo>> columnInfosMap = new ConcurrentHashMap<>();

    @NonNull
    public static List<ColumnInfo> getColumnInfos(@NonNull Class<?> tableClass) {
        List<ColumnInfo> columnInfos = columnInfosMap.get(tableClass);
        if (columnInfos == null) {
            Field[] fields = tableClass.getDeclaredFields();
            columnInfos = new ArrayList<>();
            for (Field field : fields) {
                ColumnInfo columnInfo = ColumnInfo.create(field);
                if (columnInfo == null) continue;
                columnInfos.add(columnInfo);
            }
            columnInfosMap.put(tableClass, columnInfos);
        }
        return columnInfos;
    }

}
