package com.tuuzed.android.qrcode;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.NonNull;

import com.tuuzed.android.qrcode.internal.camera.CameraManager;

class DecodeThread extends HandlerThread {
    private static final int FLAG_REQUEST_PREVIEW_FRAME = 1000;

    private final CameraManager mCameraManager;
    private final Handler mRequestPreviewFrameHandler;
    private final int mRequestPreviewFrameWhat;
    private Handler mInnerHandler;

    DecodeThread(@NonNull CameraManager cameraManager,
                 @NonNull Handler requestPreviewFrameHandler,
                 int requestPreviewFrameWhat) {
        super("DecodeThread");
        this.mCameraManager = cameraManager;
        this.mRequestPreviewFrameHandler = requestPreviewFrameHandler;
        this.mRequestPreviewFrameWhat = requestPreviewFrameWhat;
    }

    public void requestPreviewFrame() {
        mInnerHandler.sendEmptyMessage(FLAG_REQUEST_PREVIEW_FRAME);
    }

    @Override
    public synchronized void start() {
        super.start();
        mInnerHandler = new Handler(getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == FLAG_REQUEST_PREVIEW_FRAME) {
                    mCameraManager.requestPreviewFrame(mRequestPreviewFrameHandler, mRequestPreviewFrameWhat);
                }
            }
        };
    }

}