package com.tuuzed.android.sample.domain

data class ListItem(var title: String, var listener: OnListItemClickListener? = null)
